import BlinkingCell from '../components/common/blinking-cell';

export default {
    title: 'Common/BlinkingCell',
    component: BlinkingCell,
    argTypes: {
        isRandomValue: {
            control: {
                type: 'boolean'
            },
            defaultValue: false
        },
        displayChangesType: {
            control: {
                type: 'inline-radio',
                options: ['blink', 'mark']
            },
            defaultValue: 'blink'
        },
        markPartSmaller: {
            control: {
                type: 'boolean'
            },
            defaultValue: true
        },
        updateInterval: {
            control: {
                type: 'range',
                min: 300,
                max: 5000,
                step: 100
            },
            defaultValue: 500
        },
        aliasSings: {
            control: {
                type: 'number'
            },
            defaultValue: 4
        },
        blinkInterval: {
           control: {
               type: 'range',
               min: 100,
               max: 1000,
               step: 50
           },
            defaultValue: 300
        },
        value: {
            control: 'number',
            defaultValue: 34
        },
        valueMin: {
            control: 'number',
            defaultValue: 1
        },
        valueMax: {
            control: 'number',
            defaultValue: 100
        }
    },
};

export const Default = (args, { argTypes }) => ({
    props: Object.keys(argTypes),
    data() {
        return {
            randomValue: this.makeRandomValue()
        }
    },
    components: {
        BlinkingCell
    },
    computed: {
        valueToProceed() {
            if (this.isRandomValue) {
                return this.randomValue;
            } else {
                return this.value;
            }
        },
        valueToProceedAlias() {
            return this.valueToProceed.toFixed(this.aliasSings)
        }
    },
    methods: {
        removeUpdateInterval() {
            if (this.$$updateInterval) {
                clearInterval(this.$$updateInterval);
                delete this.$$updateInterval;
            }
        },
        setUpdateInterval() {
            this.removeUpdateInterval();

            this.randomValue = this.makeRandomValue();
            this.$$updateInterval = setInterval(() => {
                this.randomValue = this.makeRandomValue();
            }, this.updateInterval);
        },
        makeRandomValue() {
            return this.valueMin + (Math.random() * (this.valueMax - this.valueMin));
        }
    },
    watch: {
        updateInterval() {
            this.setUpdateInterval();
        }
    },
    mounted() {
        this.setUpdateInterval();
    },
    destroyed() {
        this.removeUpdateInterval();
    },
    template: '<blinking-cell :value="valueToProceed" :value-alias="valueToProceedAlias" :blink-interval="blinkInterval" :type="displayChangesType" :mark-part-smaller="markPartSmaller"></blinking-cell>',
});
