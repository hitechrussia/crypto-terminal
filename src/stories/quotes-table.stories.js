import AtButton from 'at-ui/src/components/button';
import QuotesTable from '../components/common/quotes-table';

import ccxt from 'ccxt';
const ccxtInstance = new ccxt.binance();

export default {
    title: 'Common/QuotesTable',
    component: QuotesTable,
    argTypes: {

    },
};

export const Default = (args, { argTypes }) => ({
    props: Object.keys(argTypes),
    data() {
        return {
            pendingTickers: false,
            tableData: []
        }
    },
    components: {
        QuotesTable,
        AtButton
    },
    computed: {

    },
    methods: {
        async downloadTickers() {
            this.pendingTickers = true;

            this.tableData = Object.values(await ccxtInstance.fetchTickers());

            this.pendingTickers = false;
        }
    },
    watch: {

    },
    template: `
        <div>
            <at-button v-if="pendingTickers" icon="icon-refresh-ccw" :disabled="true" class="mb-2">Loading...</at-button>
            <at-button v-else icon="icon-download-cloud" @click="downloadTickers" class="mb-2">Download Tickers</at-button>
            <div class="overflow-auto">
                <quotes-table :data="tableData" exchange="binance"></quotes-table>
            </div>
        </div>
    `,
});
