import * as Vue from "vue";
import App from "./App.vue";

// Vue.config.productionTip = false;

Vue.createApp(App).mount("#app");
