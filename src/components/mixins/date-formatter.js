import moment from "moment-timezone";

const GUESS_TZ = moment.tz.guess();
const DATE_FORMAT = 'YYYY-MM-DD HH:mm:ss';

export default {
    methods: {
        getFormattedDate(val) {
            return moment.utc(val).tz(GUESS_TZ).format(DATE_FORMAT);
        }
    }
}