const Color = require('color');

const PREFIXES = {
    backgroundColor: ['bg'],
    textColor: ['text'],
    borderColor: ['border', 'border-t', 'border-r', 'border-b', 'border-l'],
    svgFill: ['fill'],
    svgStroke: ['stroke']
}

const PROPERTIES = {
    backgroundColor: ['backgroundColor'],
    textColor: ['color'],
    borderColor: [
        'borderColor',
        'borderTopColor',
        'borderRightColor',
        'borderBottomColor',
        'borderLeftColor'
    ],
    svgFill: ['fill'],
    svgStroke: ['stroke']
}

module.exports = function(opts = {}) {
    return function({ e, addUtilities, config }) {
        let {
            alpha = config('alpha', config('opacity', {})),
            modules = {
                backgroundColors: true,
                textColor: false,
                borderColors: false,
                svgFill: false,
                svgStroke: false
            }
        } = opts

        Object.entries(alpha).forEach(([alphaKey, alphaValue]) => {
            let alphaValueFloat = parseFloat(alphaValue)
            if (alphaValueFloat === 0 || alphaValueFloat === 1) return null

            Object.entries(modules).forEach(([configKey, variants]) => {
                if (variants === true) {
                    // variants = config(`modules.${configKey}`, [])
                    variants = config(`variants.${configKey}`, [])
                }
                if (variants === false) return

                // let colors = config(configKey, {})
                let colors = config(`theme.${configKey}`, {});
                colors = Object.entries(colors)
                    .reduce((res, [colorKey, color]) => {
                        if (typeof color === 'string') {
                            res.push([colorKey, color]);
                        } else {
                            Object.entries(color)
                                .forEach(([subColorKey, color]) => {
                                    res.push([`${colorKey}-${subColorKey}`, color]);
                                });
                        }

                        return res;
                    }, []);

                const colorsToProceed = colors
                    .map(([colorKey, color]) => {
                        try {
                            let parsed = Color(color);
                            if (parsed.valpha === 1) {
                                return PREFIXES[configKey].map((prefix, i) => {
                                    return {
                                        [`.${e(`${prefix}-${colorKey}-${alphaKey}`)}`]: {
                                            [`${PROPERTIES[configKey][i]}`]: parsed
                                                .alpha(alphaValueFloat)
                                                .string()
                                        }
                                    }
                                })
                            }
                        } catch (err) {
                            return null
                        }
                        return null
                    })
                    .filter(Boolean);

                addUtilities(
                    colorsToProceed,
                    variants
                )
            })
        })
    }
};
