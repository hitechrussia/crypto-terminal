module.exports = {
    syntax: 'postcss-scss',
    parser: 'postcss-scss',
    plugins: [
        // ...
        require('tailwindcss'),
        require('autoprefixer'),
        // ...
    ]
}