const tailwindAlpha = require('./tailwind-plugins/tailwind-alpha');

module.exports = {
  purge: [],
  theme: {
    extend: {},
  },
  variants: {},
  future: {
    removeDeprecatedGapUtilities: true,
  },
  plugins: [
    tailwindAlpha({
      modules: {
        backgroundColor: true,
        textColor: true,
        borderColor: true
      },
      alpha: {
        '10': 0.1,
        '20': 0.2,
        '30': 0.3,
        '40': 0.4,
        '50': 0.5,
        '60': 0.6,
        '70': 0.7,
        '80': 0.8,
        '90': 0.9
      }
    })
  ]
};
