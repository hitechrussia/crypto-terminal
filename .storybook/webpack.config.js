module.exports = function ({config, mode}) {
    config.module.rules.forEach((rule) => {
        if (rule.test.toString() === '/\\.css$/') {
            rule.test = /\.[s]?css$/;
            rule.use.splice(-1,0, 'sass-loader');
        }
    })

    return config;
}
